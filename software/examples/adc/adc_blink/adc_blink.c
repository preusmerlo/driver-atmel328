// Fuente: adc_blink.c
#include <avr/io.h>
#include <util/delay.h>

void delay(uint16_t ms);						 // Prototipo de retardo bloqueante

int main(void){
	uint16_t adc = 0;	
	
	ADMUX  |= _BV(REFS0);						 // Selecciona tension de referencia	
	ADMUX  |= _BV(MUX1) | _BV(MUX0);			 // Multiplexa ADC3 al ADC (PC3)
	DIDR0  |= _BV(ADC3D);						 // Desactiva entrada digital (PC3)
	ADCSRA |= _BV(ADPS2)| _BV(ADPS1)| _BV(ADPS0);// Fija prescaler de 128
	ADCSRA |= _BV(ADEN) | _BV(ADATE);			 // Activa disparo auto y el ADC
	ADCSRA |= _BV(ADSC);						 // Inicia conversion
	DDRC   |= _BV(DDC0);						 // Define PC0 como salida

	while(1){	
		if(bit_is_set(ADCSRA,ADIF)){
			ADCSRA |= _BV(ADIF);				 // Baja bandera ADC
			adc     = (uint16_t) ADCL;			 // Lee ADC (byte menos significativo)
			adc    |= (uint16_t) (ADCH << 8);	 // Lee ADC (byte mas significativo)
		}
		PINC |= _BV(PINC0);						 // Invierte PC0
		delay(adc);								 // Genera retardo de "adc" milisegundos
	}
	return 0;
}

void delay(uint16_t ms){						 // Funcion de retardo bloqueante
  for(;ms != 0; ms--)
  	_delay_ms(1);
}
