// Fuente: adc_uart.c
#include <stdlib.h>
#include <avr/io.h>
#include <util/delay.h>

#define DELAY_MS 50
#define BAUD_RATE 0x0067						// Valor de UBRR0 para 9600 bps
#define ITOA_BASE 10							// Representacion del valor enviado por UART

int main(void){
	uint16_t adc;
	char buffer [4];
	uint8_t i;
	
	/* Inicializacion UART */	
	UBRR0H  = (uint8_t)(BAUD_RATE >> 8);		// Configura BR en byte menos significativo
	UBRR0L  = (uint8_t) BAUD_RATE;				// Configura BR en byte menos significativo
	UCSR0C |= _BV(UCSZ00) | _BV(UCSZ01);		// Define trama 8N1
	UCSR0B |= _BV(TXEN0);						// Habilita el transmisor

	/* Inicializacion ADC */
	ADMUX  |= _BV(REFS0);						// Selecciona tension de referencia	
	ADMUX  |= _BV(MUX1) | _BV(MUX0);			// Multiplexa ADC3 al ADC (PC3)
	DIDR0  |= _BV(ADC3D);						// Desactiva entrada digital (PC3)
	ADCSRA |= _BV(ADPS2)| _BV(ADPS1);			// Fija prescaler de 64
	ADCSRA |= _BV(ADEN) | _BV(ADATE);			// Activa disparo auto y el ADC
	ADCSRA |= _BV(ADSC);						// Inicia conversion

	while(1){	
		if(bit_is_set(ADCSRA,ADIF)){
			ADCSRA |= _BV(ADIF);				// Baja bandera ADC
			adc     = (uint16_t) ADCL;			// Lee ADC (byte menos significativo)
			adc    |= (uint16_t) (ADCH << 8);	// Lee ADC (byte mas significativo)
			itoa(adc,buffer,ITOA_BASE);			// Convierte int a cadena con formato decimal
		}
		
		for(i = 0; buffer[i] != '\0';i++){		// Envia cadena
			while(!(UCSR0A & _BV(UDRE0)));
			UDR0 = buffer[i];
		}
		while(!(UCSR0A & _BV(UDRE0)));
		UDR0 = '\n';							// Envia salto de linea	

		while(!(UCSR0A & _BV(UDRE0)));
		UDR0 = '\r';							// Envia retorno de carro
	
		_delay_ms(DELAY_MS);
	}
	return 0;
}
