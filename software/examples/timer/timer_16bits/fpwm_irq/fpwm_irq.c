// timer_fpwm_IR.c
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

#define CICLOS  2
#define TOP     0xFFFF
#define BOTTOM  0x0000

int main(void){
	TCCR1A |= _BV(WGM13) | _BV(WGM12) | _BV(WGM11) | _BV(WGM10);	// Configura modo Fast PWM (WGM=15)

	TCCR1A |= _BV(COM1A1)| _BV(COM1A0);	// Configura PWM  invertido
	DDRB   |= _BV(DDB1);      		// Configura PB1 (OC1A) como salida
	TIMSK1 |= _BV(TOIE1);               	// Habilita interrupciones por desborde
	sei();			                // Habilita interrupciones en general
	TCCR1B |= _BV(CS11);			// Configura frecuencia en 7.8kHz (N = 8)

	while (1){
	}
}

ISR (TIMER1_OVF_vect){
	static uint8_t cont;			// Contador para el retardo
	static uint8_t dir;			// Dirección de cambio  OCR0A
	cli();					// Deshabilita interrupciones
	cont ++;
	if (cont == CICLOS){
		dir   = (OCR1A == TOP   )? 1: dir;	// Cambia dirección al llegar a TOP
		dir   = (OCR1A == BOTTOM)? 0: dir;	// Cambia dirección al llegar a BOTTOM
		OCR1A = (dir)? OCR1A - 1: OCR1A +1;	// Incrementa o decrementa en función de dir
		cont  = 0;				// Restablece contador de ciclos
	}
	sei();						// Habilita interrupciones

}

