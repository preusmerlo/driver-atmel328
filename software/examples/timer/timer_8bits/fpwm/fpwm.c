// timer_fpwm.c
#include <avr/io.h>
#include <util/delay.h>

#define DELAY 	2
#define TOP		0xFF	
#define BOTTOM  0x00

int main(void){	
	TCCR0A |= _BV(WGM01) | _BV(WGM00);			// Configura modo Fast PWM
	TCCR0A |= _BV(COM0A1)|_BV(COM0A0);			// Configura PWM  invertido
	TCCR0B |= _BV(CS01);						// Configura frecuencia en 7.8kHz
	DDRD   |= _BV(DDD6);      					// Configura PD6 (OC0A) como salida
	
	uint8_t val;								// Valor actual de OCR0A
	while (1){
		for (val = BOTTOM; val != TOP; val++){	// Lleva el PWM de 0% a 100%
			OCR0A = val;
			_delay_ms(DELAY);
		}

		for (val = TOP; val != BOTTOM; val--){	// Lleva el PWM de 100% a 0%
			OCR0A = val;
			_delay_ms(DELAY);
		}	
	}
}

