// timer_normal.c
#define F_CPU 16000000UL
#include <avr/io.h>

#define CICLOS 12									// Ciclos para retardo de 200ms (aprox)

int main(void){	
	DDRC 	|= _BV(DDC0);      						// Configura PC0 como salida	
    TCCR0B 	|= _BV(CS02) | _BV(CS00);				// Define prescaler de 1024
	uint8_t cont;
	while (1){	
		for (cont = 0; cont < CICLOS; cont++){		// Repite CICLOS veces el desborde
        	while ( (TIFR0 & _BV(TOV0) ) == 0);   	// Espera desborde del contador
        	TIFR0 = _BV(TOV0);						// Restablece la bandera TOV0
		}
		PINC = _BV(PINC0);  						// Toggle del bit PC0
    }
}
