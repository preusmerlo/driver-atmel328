// timer_FPWM.c
#define F_CPU 16000000UL

#include <avr/io.h>
#include <util/delay.h>

#define DELAY 	2
#define TOP		255	

int main(void){	
	TCCR0A |= _BV(WGM00);				// Configura modo Phace Correct PWM
	TCCR0A |= _BV(COM0A1)|_BV(COM0A0);	// Configura PWM  invertido
	TCCR0B |= _BV(CS01);				// Configura frecuencia en 7.8kHz
	OCR0A 	= 0X00; 					// Fija PWM al 0%
	DDRD   |= _BV(DDD6);      			// Configura PD6 (OC0A) como salida
	
	uint8_t i;
	while (1){
		for (i = 0; i != TOP; i++){		// Lleva el PWM de 0% a 100%
			OCR0A = i;
			_delay_ms(DELAY);
		}

		for (i = TOP; i != 0; i--){		// Lleva el PWM de 100% a 0%
			OCR0A = i;
			_delay_ms(DELAY);
		}	
	}
}

