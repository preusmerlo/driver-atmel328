// timer_CTC.c
#include <avr/io.h>

int main(void){	
    /* Generador de señal cuadrada de 30Hz en PD6 */
	TCCR0A |= _BV(WGM01) | _BV(COM0A0);	// Configura modo CTC y toggle de OC0A
	OCR0A 	= 0XFF; 					// Define TOP en OCR0A en 255 (MAX)
	DDRD   |= _BV(DDD6);      			// Configura PD6 (OC0A) como salida
	TCCR0B |= _BV(CS02) | _BV(CS00);	// Configura prescaler de 1024
	
	while (1){
	}
}
