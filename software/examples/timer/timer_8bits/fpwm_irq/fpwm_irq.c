// timer_fpwm_IR.c
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

#define CICLOS  16
#define TOP		0xFF	
#define BOTTOM  0x00

int main(void){	
	TCCR0A |= _BV(WGM01) | _BV(WGM00);		// Configura modo Fast PWM
	TCCR0A |= _BV(COM0A1)|_BV(COM0A0);		// Configura PWM  invertido
	DDRD   |= _BV(DDD6);      				// Configura PD6 (OC0A) como salida
	TIMSK0 |= _BV(TOIE0);               	// Habilita interrupciones por desborde
	sei();			                        // Habilita interrupciones en general		
	TCCR0B |= _BV(CS01);					// Configura frecuencia en 7.8kHz (N = 8)

	while (1){
	}
}

ISR (TIMER0_OVF_vect){
	static uint8_t cont;					// Contador para el retardo
	static uint8_t dir;						// Dirección de cambio  OCR0A
	cli();									// Deshabilita interrupciones
	cont ++;
	if (cont == CICLOS){
		dir   = (OCR0A == TOP   )? 1: dir;	// Cambia dirección al llegar a TOP
		dir   = (OCR0A == BOTTOM)? 0: dir;	// Cambia dirección al llegar a BOTTOM
		OCR0A = (dir)? OCR0A - 1: OCR0A +1;	// Incrementa o decrementa en función de dir
		cont  = 0;							// Restablece contador de ciclos	
	}
	sei();									// Habilita interrupciones
} 

