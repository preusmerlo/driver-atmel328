// Fuente: slave.c
#include <avr/io.h>

int main(void){
	DDRB |= _BV(DDB4); 						 // Define MISO (PB4) como salida
	SPCR |= _BV(SPE);						 // Habilita el SPI
	DDRC |= _BV(DDC0);						 // Define PC0 como salida	
	char dato;

	while(1){
		while(! bit_is_set(SPSR,SPIF));		 // Espera fin de recepcion
		dato = SPDR;						 // Lee dato
		
		switch(dato){
			case 'A': PORTC |=  _BV(PORTC0); // Pone en uno PC0  si dato = 'A'
					  break;
			case 'B': PORTC &= ~_BV(PORTC0); // Pone en cero PC0 si dato = 'B'
					  break;
		}
	}
	return 0;
}


