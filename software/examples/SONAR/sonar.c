// sonar.c
#include <avr/io.h>
#include <util/delay.h>
#include <stdlib.h>

#define BAUD_RATE 0x0067		// Define macro con BR de 9600

int main(void){

	uint16_t ciclos = 0;
	uint16_t tiempo = 0;
	uint16_t distancia = 0;
	char buffer [4];
	uint8_t i;
	uint8_t final[] = " cm \n\r";		// Cadena de finalización

	//Configuración SONAR
	DDRD |= _BV(DDD6);      	// Configura PD6 como salida (Trigger Sonar)

	//Configuración UART
   	UBRR0H  = (uint8_t)(BAUD_RATE >> 8); 	// Configura BR (MSByte)
	UBRR0L  = (uint8_t) BAUD_RATE;		// Configura BR (LSByte)
	UCSR0C  = _BV(UCSZ00) | _BV(UCSZ01); 	// Define trama 8N1
	UCSR0B |= _BV(TXEN0); 			// Habilita el transmisor

	//Configuración Timer0 (8bits)
	TCCR0B 	|= _BV(CS00);			// Define prescaler de 1

    	while (1){
 		PORTD |= _BV(PORTD6);		// Pone en bajo D6
		_delay_us(4);			// Espera 4us
		PORTD &= ~_BV(PORTD6);		// Pone en alto D6
		_delay_us(10);			// Espera 10us
		PORTD |= _BV(PORTD6);		// Pone en bajo D6

		while(((PIND >> PIND5) & 0X01) != 0X01);	//Espera que Echo (PD5) se ponga en alto

		while(((PIND >> PIND5) & 0X01) == 0X01){	//Verifica que Echo esté en alto (PD5)
			while((TIFR0 & _BV(TOV0)) == 0);	// Espera desborde del contador
			TIFR0 = _BV(TOV0);			// Restablece bandera TOV0
			ciclos++;				// Aumenta contador
		}

		tiempo = ciclos * 16;		// Cada ciclo son 16us
		ciclos = 0;

		distancia = tiempo *5 /292;	// Convierto de us a cm

		itoa(distancia,buffer,10);	//Convierte int a cadena con formato decimal

		for(i=0; buffer[i] != '\0'; i++){
			while(!(UCSR0A & _BV(UDRE0)));	// Espera bufer de transmision vacío
			UDR0 = buffer[i];		// Escribe el bufer de transmisión
    		}

		for(i=0; final[i] != '\0'; i++){
			while(!(UCSR0A & _BV(UDRE0)));	// Espera bufer de transmision vacío
			UDR0 = final[i];		// Escribe el bufer de transmisión
    		}
		_delay_ms(1000);
	}
    return 0;
}
