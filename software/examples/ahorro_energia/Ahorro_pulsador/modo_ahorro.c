// Fuente: modo_ahorro.c
#include <avr/io.h>
#include <avr/sleep.h>
#include <avr/interrupt.h>
#include <util/delay.h>

int main(void){

	DDRC |= _BV(DDC0);	// PC0 como salida (prende LED)
	PINC |= _BV(PINC0);	// PC0 en alto (apaga LED para que comience off)

	EICRA |= _BV(ISC11);	// Configura evento: flanco descendente
	EIMSK |= _BV(INT1);	// Habilita interrupcion INT1
	sei();			// Habilita interrupción en general

	SMCR |= _BV(SE);	// Habilita modos de suspensión

	while(1){
		sleep_cpu();	// Instrucción de suspensión
	}

	return 0;
}

ISR (INT1_vect){

	SMCR |= _BV(SE);	// Deshabilita modos de suspensión

	PINC |= _BV(PINC0);	// prende LED
	_delay_ms(5000);
	PINC |= _BV(PINC0);	// apaga LED

	SMCR |= _BV(SE);	// Habilita modos de suspensión
}
