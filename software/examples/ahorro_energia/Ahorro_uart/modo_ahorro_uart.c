// Fuente: modo_ahorro_uart.c
#include <avr/io.h>
#include <avr/sleep.h>
#include <avr/interrupt.h>
#include <util/delay.h>

#define BAUD_RATE 0x0067 	// Valor de UBRR0 para 9600 bps

int main(void){

	uint8_t car;
	uint8_t mensaje[] = "El equipo entró y salió de la suspensión \n\r";
	int i;

	UBRR0H = (uint8_t)(BAUD_RATE >> 8);	//Configura BR (MSB)
	UBRR0L = (uint8_t)BAUD_RATE;		//Configura BR (LSB)
	UCSR0C = _BV(UCSZ00) | _BV(UCSZ01);	//Define trama 8N1
	UCSR0B |= _BV(TXEN0);			//Habilita transmisor
	UCSR0B |= _BV(RXEN0);			//Habilita receptor

	sei();					//Habilita interrupción en general
	TCCR2B |= _BV(CS22) | _BV(CS20);	//Define prescaler de 1024 en Timer2

	SMCR |=  _BV(SM1) |_BV(SM0);		//Modo Power-save (SM[2:0]=011)

	while(1){

		while(!(UCSR0A & _BV(RXC0)));	//Espera dato en bufer de recepción
		car = UDR0;			//Lee el bufer de recepción

		if( car=='S' || car=='s'){	//Pregunta por 's' o 'S'
			SMCR |= _BV(SE);	//Habilita modos de suspensión
			TIMSK2 |= _BV(TOIE2);	//Habilita interrupciones por desborde Timer2
			TCNT2 = 0;		//Resetea el contador 2
			sleep_cpu();		//Instrucción de suspensión

			for(i=0; mensaje[i] != '\0'; i++){
				while(!(UCSR0A & _BV(UDRE0)));	//Espera bufer de transmision vacio
				UDR0 = mensaje[i];		//Escribe el bufer de transmision
			}
		}
	}

	return 0;
}

ISR (TIMER2_OVF_vect){
	SMCR |= _BV(SE);			// Deshabilita modos de suspensión
	TIMSK2 |= _BV(TOIE2);			// Deshabilita interrupciones por desborde Timer2
}
