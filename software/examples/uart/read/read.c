// Fuente: read.c
#include <avr/io.h>

#define BAUD_RATE 0x0067					// Valor de UBRR0 para 9600 bps

int main(void){
	DDRD   |= _BV(DDD6);					// Configura PD6 como salida
	DDRC   |= _BV(DDC0);					// Condigura PC0 como salida

	UBRR0H  = (uint8_t)(BAUD_RATE >> 8); 	// Configura BR (byte mas significativo)
	UBRR0L  = (uint8_t) BAUD_RATE; 			// Configura BR (byte menos significativo)
	UCSR0C  = _BV(UCSZ00) | _BV(UCSZ01);	// Define trama 8N1
	UCSR0B |= _BV(RXEN0);	 				// Habilita el transmisor

	while (1){
		while ( !( UCSR0A & _BV(RXC0)));	// Espera recibir dato
		switch(UDR0){
			case 'r':
				PIND |= _BV(PIND6);			// Invierte estado de PD6
				break;
			case 'v':
				PINC |= _BV(PINC0);			// Invierte estado de PC0
				break;
		}
	}
 	return 0;
}
