// Fuente: write_irq.c

#include <util/delay.h>
#include <avr/interrupt.h>
#include <string.h>

#define BAUD_RATE 0x0008							// Define macro con BR de 115200
#define DELAY_MS 1000

struct uart{
	char data [80];
	int  send;
};
static volatile struct uart buffer;

int main(){
	UBRR0H  = (BAUD_RATE >> 8); 					// Configura BR en byte mas significativo
	UBRR0L  =  BAUD_RATE;							// Configura BR en byte menos significativo
	UCSR0C  = _BV(UCSZ00) | _BV(UCSZ01);			// Define trama 8N1
	UCSR0B |= _BV(TXEN0); 							// Habilita el transmisor
	sei();											// Habilita interrupciones en general
	UCSR0B |= _BV(UDRIE0);							// Habilita interrupciones cuando se vacía el bufer
	
	DDRC |= _BV(DDC0);								// Define PC0 como salida
	
	strcpy((char *)buffer.data,"Hola Mundo :)\n\r");// Carga de la cadena a enviar
	buffer.send = 1;
	
	while(1){	
		PINC |= _BV(PINC0);							// Invierte estado de PC0
		_delay_ms(DELAY_MS);	
	}
	return 0;
}

ISR(USART_UDRE_vect){
	static uint8_t i;
	if (buffer.data[i] != '\0' && buffer.send != 1){
		UDR0 = buffer.data[i];						// Escribe el bufer de transmisión
		i++;
	}
	else{
		i = 0;
		buffer.send = 0;
	}
}
