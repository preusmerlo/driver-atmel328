// blink.c
#define F_CPU 16000000UL
#include <avr/io.h>
#include <util/delay.h>

#define DELAY 100

int main(void){
    DDRC |= _BV(DDC0);      // Configura PC0 como salida
    
    while (1){
	PINC = _BV(PINC0);  	// Toggle del bit PC0
	_delay_ms(DELAY);		// Retardo bloqueante de 100ms
    }
    return 0;
}
