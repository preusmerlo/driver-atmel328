// pulsador.c
#define F_CPU 16000000UL
#include <avr/io.h>
#include <util/delay.h>

int main(void){
    DDRB |= _BV(DDB0);      	// Configura PC0 como salida
    DDRC &= ~_BV(DDC3);      	// Configura PD5 como entrada
    
    PORTB &= ~_BV(PORTB0);	// Pone en bajo el led
	
    while (1){
    	if ((PIND >> PIND3) & 0X01 == 0X01)
		PORTB |= _BV(PORTB0);		// Pone en alto el led
	else	
		PORTB &= ~_BV(PORTB0);		// Pone en bajo  el led
    }
    return 0;
}
