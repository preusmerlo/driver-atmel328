// eeprom_lectura.c
#include <avr/io.h>
#include <util/delay.h>

#define BAUD_RATE 0x0067			// Define macro con BR de 9600

int main(void){

	int adress = 0;				// Dirección de memoria a leer
	char data;				// Caracter a leer

	//Configuración UART
   	UBRR0H  = (uint8_t)(BAUD_RATE >> 8); 	// Configura BR (MSByte)
	UBRR0L  = (uint8_t) BAUD_RATE;		// Configura BR (LSByte)
	UCSR0C  = _BV(UCSZ00) | _BV(UCSZ01); 	// Define trama 8N1
	UCSR0B |= _BV(TXEN0); 			// Habilita el transmisor

	//Lectura de EEPROM
	while(EECR & _BV(EEPE));		// Verifica que EEPE esté en 0
	EEAR = adress;				// Dirección de memoria a escribir
	EECR |= _BV(EERE);			// Pone en alto EEMPE
	data = EEDR;				// Lectura de EEPROM

	while(1){

		//Escritura en UART
		while(!(UCSR0A & _BV(UDRE0)));	// Espera bufer de transmisión vacío
		UDR0 = data;			// Escribe el bufer de transmisión

		while(!(UCSR0A & _BV(UDRE0)));	// Espera bufer de transmisión vacío
		UDR0 = '\n';			// Escribir el bufer de transmisión

		while(!(UCSR0A & _BV(UDRE0)));	// Espera bufer de transmisión vacío
		UDR0 = '\r';			// Escribir el bufer de transmisión

		_delay_ms (1000);
	}

	return 0;
}
